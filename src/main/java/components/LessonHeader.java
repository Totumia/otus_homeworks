package components;

import data.PageHeadersData;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LessonHeader<T> extends AbsComponents<LessonHeader> {
  private String lessonHeaderLocator = "//h1";

  public LessonHeader(WebDriver driver) {
    super(driver);
  }

  public T checkLessonHeader(PageHeadersData pageHeadersData) {
    WebElement lessonHeader = $(By.xpath(lessonHeaderLocator));
    Assertions.assertEquals(lessonHeader.getText(), pageHeadersData.getName());
    return (T) this;
  }
}
