package data;

public enum LessonsData {
  ApplicationSoftwareDevelopmentOnQAndAuroraOS("Разработка прикладного ПО на Qt и ОС Аврора"),
  DevRel("DevRel"),
  BusinessAnalystAndSystemsAnalyst("Бизнес-аналитик и системный аналитик"),
  AndroidSpecialization("Специализация Android"),
  JavaDeveloperSpecialization("Специализация Java-разработчик"),
  QaAutomationSpecialization("Специализация QA Automation Engineer"),
  FullstackDeveloperSpecialization("Специализация Fullstack Developer"),
  CPlusSpecialization("Специализация С++"),
  CSharpSpecialization("Специализация С#"),
  AdministratorLinuxSpecialization("Специализация Administrator Linux"),
  SystemAnalystSpecialization("Специализация Системный аналитик"),
  PythonSpecialization("Специализация Python"),
  MachineLearningSpecialization("Специализация Machine Learning"),
  IOSSpecialization("Специализация iOS Developer"),
  NetworkEngineerSpecialization("Специализация сетевой инженер"),
  SystemAnalyticBasic("Системный аналитик. Basic"),
  MlOps("MLOps"),
  PythonDevBase("Python Developer. Basic");

  private String name;

  LessonsData(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
