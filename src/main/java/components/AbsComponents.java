package components;

import org.openqa.selenium.WebDriver;
import pageobject.PageObject;

public abstract class AbsComponents<T> extends PageObject<T> {
  public AbsComponents(WebDriver driver) {
    super(driver);

  }

}
