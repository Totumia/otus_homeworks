package pages;

import annotations.Path;
import data.LessonsData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Path("/")
public class MainPage extends BasePage<MainPage> {
  public MainPage(WebDriver driver) {
    super(driver);
  }

  private String lessonsLocator = "//div[contains(text(),'%s')]/ancestor::a";


  public MainPage moveToLesson(LessonsData lessonsData) {
    String selector = String.format(lessonsLocator, lessonsData.getName());
    this.actions.moveToElement($(By.xpath(selector))).click().build().perform();
    return this;
  }

}
