package lessons;

import annotations.Driver;
import components.LessonsComponent;
import components.LessonHeader;
import data.LessonsData;
import data.PageHeadersData;
import extensions.UIExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import pages.MainPage;

@ExtendWith(UIExtension.class)
public class Lessons_Test {
  @Driver
  private WebDriver driver;


  @Test
  public void searchLessonByName() {
    new MainPage(driver)
        .open()
        .headerShouldBeTheSameAs(PageHeadersData.MainPageHeader);
    new LessonsComponent(driver)
        .searchLessonByName(LessonsData.ApplicationSoftwareDevelopmentOnQAndAuroraOS);
    new LessonHeader(driver)
        .checkLessonHeader(PageHeadersData.ApplicationSoftwareDevelopmentOnQAndAuroraOSHeader);
  }

  @Test
  public void checkClickEarliestCourse() {
    new MainPage(driver)
        .open()
        .headerShouldBeTheSameAs(PageHeadersData.MainPageHeader);
    new LessonsComponent(driver)
        .getEarlierLastCourseItem(LessonsComponent::findEarliestCourse)
        .headerShouldBeTheSameAs(PageHeadersData.SystemAnalyticBasicHeader);
  }

  @Test
  public void checkClickLatestCourse() {
    new MainPage(driver)
        .open()
        .headerShouldBeTheSameAs(PageHeadersData.MainPageHeader);
    new LessonsComponent(driver)
        .getEarlierLastCourseItem(LessonsComponent::findLatestCourse)
        .headerShouldBeTheSameAs(PageHeadersData.CSharpSpecializationHeader);
  }


}
