package data;

public enum PageHeadersData {
  MainPageHeader("Авторские онлайн‑курсы для профессионалов"),
  ApplicationSoftwareDevelopmentOnQAndAuroraOSHeader("Разработка прикладного ПО на Qt и ОС Аврора"),
  DevRelHeader("Узнайте, что такое Developer Relations и как запустить их самостоятельно."),
  BusinessAnalystAndSystemsAnalystHeader("Бизнес-аналитик и системный аналитик"),
  JavaDeveloperSpecializationHeader("Специализация Java-разработчик"),
  AndroidSpecializationHeader("Android Developer"),
  QaAutomationSpecializationHeader("QA Automation" + "\n" + "Engineer"),
  FullstackDeveloperSpecializationHeader("Fullstack" + "\n" + "Developer"),
  CPlusSpecializationHeader("C++ DEVELOPER"),
  CSharpSpecializationHeader("C# Developer"),
  AdministratorLinuxSpecializationHeader("Administrator Linux"),
  SystemAnalystSpecializationHeader("Системный аналитик"),
  PythonSpecializationHeader("Python Developer"),
  MachineLearningSpecializationHeader("Machine Learning"),
  IOSSpecializationHeader("iOS Developer"),
  NetworkEngineerSpecialization("Network Engineer"),
  MLOpsHeader("MLOps"),
  SystemAnalyticBasicHeader("Системный аналитик. Basic"),
  PythonDevBaseHeader("Разработчик Python. Базовый уровень");


  private String name;

  PageHeadersData(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
