package pageobject;

import data.PageHeadersData;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public abstract class PageObject<T> {
  protected WebDriver driver;
  protected Actions actions;


  public PageObject(WebDriver driver) {
    this.driver = driver;
    this.actions = new Actions(driver);
    PageFactory.initElements(driver, this);

  }

  public WebElement $(By locator) {
    return driver.findElement(locator);
  }

  @FindBy(tagName = "h1")
  private WebElement header;


  public T headerShouldBeTheSameAs(PageHeadersData header) {
    Assertions.assertEquals(header.getName(), this.header.getText());
    return (T) this;
  }


}
