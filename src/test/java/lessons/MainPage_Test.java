package lessons;

import annotations.Driver;
import data.LessonsData;
import data.PageHeadersData;
import extensions.UIExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import pages.MainPage;


@ExtendWith(UIExtension.class)
public class MainPage_Test {
  @Driver
  private WebDriver driver;

  @Test
  public void checkMoveToSelectedCourse() {
    new MainPage(driver)
        .open()
        .headerShouldBeTheSameAs(PageHeadersData.MainPageHeader)
        .moveToLesson(LessonsData.IOSSpecialization)
        .headerShouldBeTheSameAs(PageHeadersData.IOSSpecializationHeader);
  }


}
